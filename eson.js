function CharacterSource(textSource) {
    let i = 0
    this.nextCharacter = ()=>{
        while (1) {
            const character = textSource[i]
            if (character === undefined) {
                return null
            } else {
                i++
                return character
            }
        }
    }
    this.clear = ()=> {
      i = 0
    }
    this.setText = text=> {
      textSource = text
    }
}

function EsonParser(characterSource) {
    const SPACE = ' '
    const LIST = 'list'
    const MAP = 'map'
    const EOF = null
    const LINE_FEED = '\n'
    const NULL = null
    const DOT = '.'
    const DASH = '-'
    const BACKSLASH = '\\'
    const SHARP = '#'

    let context = LIST
    let stack = [context]
    let lvl = 0
    let maxLvl = 0
    let lastContext = NULL
    let nextContext = NULL
    let cnt = 0

    let c = null
    let line = 1
    let column = 0
    let initialized = false
    let esonStreamMessagesReceiver = null;

    let n = ()=>{
        c = characterSource.nextCharacter()
        if (c === '\n') {
            column = 0
            line++
        } else if(c !== EOF) {
            column++
        }
    }

    let readUntilEndOfLine = ()=>{
        while (!(c === LINE_FEED || c === EOF)) {
            esonStreamMessagesReceiver.character(c)
            n()
        }
    }

    let escape = characters=>{
        if (c === BACKSLASH) {
            n()
            while (c === BACKSLASH) {
                esonStreamMessagesReceiver.character(BACKSLASH)
                n()
            }
            if (characters.indexOf(c) === -1) {
                esonStreamMessagesReceiver.character(BACKSLASH)
            }
        }
    }

    let readUntilSpace = ()=>{
        while (!(c === LINE_FEED || c === EOF)) {
            esonStreamMessagesReceiver.character(c)
            n()
            if (c === SPACE) {
                n()
                break
            }
            escape([SPACE])
        }
    }

    let printLocation = ()=>{
        return line + ':' + column + ' '
    }

    let throwError = message=>{
        throw new Error(printLocation() + message)
    }

    let end = ()=>{
        for (; stack.length > 1; )
            pop()
    }

    let leftShift = ()=>{
        esonStreamMessagesReceiver.leftShift(lvl - cnt)
        do {
            lvl--
            stack.pop()
            context = stack[stack.length - 1]
        } while (cnt < lvl)
    }

    this.parseLine = ()=>{
        if (!initialized) {
            n()
            if (c === SPACE) {
                throwError("Didn't expect space")
            }
            initialized = true
        }
        if (c === DOT || c === DASH) {
            if (c === DOT) {
                nextContext = LIST
            } else if (c === DASH) {
                nextContext = MAP
            }
            n()
            lastContext = context
            context = nextContext
            stack.push(context)
            if (lastContext === LIST) {
                if (c === LINE_FEED || c === EOF) {
                    if (nextContext === MAP) {
                        esonStreamMessagesReceiver.addMapToList()
                    } else if (nextContext === LIST) {
                        esonStreamMessagesReceiver.addListToList()
                    }
                } else {
                    throw new Error(printLocation() + "Expected LINE_FEED but got " + c)
                }
            } else if (lastContext === MAP) {
                if (nextContext === MAP) {
                    esonStreamMessagesReceiver.startKeyForMap()
                    readUntilEndOfLine()
                    esonStreamMessagesReceiver.endKeyForMap()
                } else if (nextContext === LIST) {
                    esonStreamMessagesReceiver.startKeyForList()
                    readUntilEndOfLine()
                    esonStreamMessagesReceiver.endKeyForList()
                }
            }
        } else if (c === SHARP) {
            while (1) {
                n()
                while (c !== LINE_FEED) {
                    if (c === EOF) {
                        esonStreamMessagesReceiver.end()
                        return false
                    }
                    n()
                }
                n()
                cnt = 0
                let result = 0
                while (1) {
                    if (c === SPACE) {
                        cnt++
                        if (cnt > lvl) {
                            break
                        } else {
                            n()
                        }
                    } else {
                        result = 1
                        break
                    }
                }
                if (result === 1) {
                    break
                }
            }
            if (cnt < lvl) {
                esonStreamMessagesReceiver.leftShift(lvl - cnt)
                do {
                    lvl--
                    stack.pop()
                    context = stack[stack.length - 1]
                } while (cnt < lvl)
            }
            return true
        } else {
            if (context === LIST) {
                esonStreamMessagesReceiver.startValueToList()
                escape([SPACE, SHARP, DASH, DOT])
                readUntilEndOfLine()
                esonStreamMessagesReceiver.endValueToList()
            } else if (context === MAP) {
                esonStreamMessagesReceiver.startKeyForValue()
                escape([SPACE, SHARP, DASH, DOT])
                readUntilSpace()
                esonStreamMessagesReceiver.endKeyForValue()
                esonStreamMessagesReceiver.startValueToMap()
                readUntilEndOfLine()
                esonStreamMessagesReceiver.endValueToMap()
            }
        }
        if (c === EOF) {
            esonStreamMessagesReceiver.end()
            return false
        } else {
            n()
            if (nextContext !== NULL) {
                maxLvl = lvl + 1
            } else {
                maxLvl = lvl
            }
            cnt = 0
            while (c === SPACE) {
                cnt++
                if (cnt > maxLvl) {
                    throw new Error(printLocation() + "Expected at most " + maxLvl + " spaces")
                } else {
                    n()
                }
            }
            if (cnt <= lvl) {
                if (nextContext != NULL) {
                    esonStreamMessagesReceiver.emptyParent()
                    stack.pop()
                    context = stack[stack.length - 1]
                }
                if (cnt < lvl) {
                    esonStreamMessagesReceiver.leftShift(lvl - cnt)
                    do {
                        lvl--
                        stack.pop()
                        context = stack[stack.length - 1]
                    } while (cnt < lvl)
                }
            } else {
                lvl++
            }
            nextContext = NULL
        }
        return true
    }

    this.nextEsonStreamMessages = esonStreamMessagesReceiver_param=> {
      esonStreamMessagesReceiver = esonStreamMessagesReceiver_param
      this.parseLine()
    }

    this.clear = ()=> {
      context = LIST
      stack = [context]
      lvl = 0
      maxLvl = 0
      lastContext = NULL
      nextContext = NULL
      cnt = 0

      c = null
      line = 1
      column = 0
      initialized = false
      esonStreamMessagesReceiver = null;
      characterSource.clear()
    }
}

function EsonStreamMessagesToEsonBufferedMessagesTransformer(esonStreamMessagesSource) {
    // private state
    let storedBuffer = ''
    let buffer = ''
    let esonBufferedMessagesReceiver = null
    let drained = false

    // private functions
    let clearBuffer = ()=>buffer = ''



    // messages to transform
    this.leftShift = n=>esonBufferedMessagesReceiver.leftShift(n)
    this.startValueToList = clearBuffer
    this.endValueToList = ()=>esonBufferedMessagesReceiver.addValueToList(buffer)
    this.startKeyForValue = clearBuffer
    this.endKeyForValue = ()=>storedBuffer = buffer
    this.startValueToMap = clearBuffer
    this.endValueToMap = ()=>esonBufferedMessagesReceiver.addValueToMap(storedBuffer, buffer)
    this.addListToList = ()=>esonBufferedMessagesReceiver.addListToList()
    this.startKeyForList = clearBuffer
    this.endKeyForList = ()=>esonBufferedMessagesReceiver.addListToMap(buffer)
    this.addMapToList = ()=>esonBufferedMessagesReceiver.addMapToList()
    this.startKeyForMap = clearBuffer
    this.endKeyForMap = ()=>esonBufferedMessagesReceiver.addMapToMap(buffer)
    this.emptyParent = ()=>esonBufferedMessagesReceiver.emptyParent()
    this.end = ()=>esonBufferedMessagesReceiver.end()
    this.character = character=>buffer += character

    this.nextEsonBufferedMessages = esonBufferedMessagesReceiver_param=> {
      esonBufferedMessagesReceiver = esonBufferedMessagesReceiver_param

      esonStreamMessagesSource.nextEsonStreamMessages(this)
    }

    this.clear = ()=> {
         storedBuffer = ''
         buffer = ''
         esonBufferedMessagesReceiver = null
         drained = false
         esonStreamMessagesSource.clear()
    }
}


function EsonBufferedMessagesToObjectTransformer(esonBufferedMessagesSource) {

    // private state
    let _top = []
    let stack = [_top]
    let nextObject = null
    let drained = false

    // private functions
    let push = parent=>{
        stack.push(parent)
        _top = parent
    }
    let pop = ()=>{
        if (stack.length === 2) {
            nextObject = stack[1]
        }
        stack.pop()
        _top = stack[stack.length - 1]
    }
    let addParentToList = parent=>{
        _top.push(parent)
        push(parent)
    }
    let addParentToMap = (key,parent)=>{
        _top[key] = parent
        push(parent)
    }

    // messages to transform
    this.leftShift = n=>{
        for (; n > 0; n--)
            pop()
    }
    this.addValueToList = value=>{
      if(stack.length === 1) {
        nextObject = value
      }
      _top.push(value)
    }
    this.addValueToMap = (key,value)=>_top[key] = value
    this.addListToList = ()=>addParentToList([])
    this.addListToMap = key=>addParentToMap(key, [])
    this.addMapToList = ()=>addParentToList({})
    this.addMapToMap = key=>addParentToMap(key, {})
    this.emptyParent = pop
    this.end = ()=>{
        for (; stack.length > 1; )
            pop()
        drained = true
    }

    this.nextObject = ()=> {
      nextObject = null
      // loop while nextObject is null and not drained
      while(nextObject === null && !drained) {
        esonBufferedMessagesSource.nextEsonBufferedMessages(this)
      }

      return nextObject
    }

    this.clear = ()=> {
      _top = []
      stack = [_top]
      nextObject = null
      drained = false
      esonBufferedMessagesSource.clear()
    }
}

function ObjectCollector(objectSource) {
  this.getObjectList = ()=>{
    let list = []
    let object = null
      while((object = objectSource.nextObject()) !== null) {
        list.push(object)
      }
      return list
  }
  this.clear = ()=> {
    objectSource.clear();
  }
}


let testString = `-
 myKey myValue
 .empty list
 anotherKey anotherValue
 #ignored line
 -key for a sub map
  key value
  a b
  #ignored sub tree
   this is a comment
   over several lines
  .listKey
   1
   #2
   3
 .a list in a list
  .`

let characterSource = new CharacterSource(testString)

let objectCollector = new ObjectCollector(
  new EsonBufferedMessagesToObjectTransformer(
    new EsonStreamMessagesToEsonBufferedMessagesTransformer(
      new EsonParser(
        characterSource
      )
    )
  ));

function convert() {
  return JSON.stringify(objectCollector.getObjectList(), null, 4)
}

let output = convert()
let errorText = document.createTextNode('')
errorText.nodeValue = 'Successfully parsed'

let divElement = document.createElement('div')
divElement.style.position = 'relative'
let header1Element = document.createElement('h2')
let header1Txt = document.createTextNode('Input:')
header1Element.appendChild(header1Txt)
divElement.appendChild(header1Element)
let errorElement = document.createElement('div')

errorElement.appendChild(errorText)
divElement.appendChild(errorElement)
let inputElement = document.createElement('textarea')
inputElement.style.width = '100%'
inputElement.style.height = '290px'
inputElement.style.whiteSpace = 'pre'
let inputText = document.createTextNode(testString)
let outputText = document.createTextNode(output)
inputElement.addEventListener("input", function() {
    objectCollector.clear()
    characterSource.setText(inputElement.value)
    try {
    output = convert()
    errorText.nodeValue = 'Successfully parsed'
  } catch(e) {
    errorText.nodeValue = e.message
  }
    outputText.nodeValue = output
}, false);
inputElement.appendChild(inputText)
divElement.appendChild(inputElement)
let editMeElement = document.createElement('aside')
editMeElement.style.position = 'absolute'
editMeElement.style.right = '-80px'
editMeElement.style.top= '70px'
editMeElement.textContent = '← Edit me!'
divElement.appendChild(editMeElement)
let header2Element = document.createElement('h2')
let header2Txt = document.createTextNode('Output:')
header2Element.appendChild(header2Txt)
divElement.appendChild(header2Element)
let outputElement = document.createElement('pre')

outputElement.appendChild(outputText)
divElement.appendChild(outputElement)
document.body.appendChild(divElement)

console.log("Input:")
console.log(testString)
console.log("Output:")
console.log(output)
